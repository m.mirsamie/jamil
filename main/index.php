<?php
	include_once("../kernel.php");
	include_once("../simplejson.php");
	$SESSION = new session_class;
	register_shutdown_function('session_write_close');
	session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die('<script>window.location="admin_login.php";</script>');//die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
               die('<script>window.location="admin_login.php";</script>');//die($conf->access_deny);
	$user_id=trim((int)$_SESSION[$conf->app.'_user_id']);
	$start1 = strtotime((trim($conf->start1)!='')?$conf->start1:'11:00');
	$stop1 = strtotime((trim($conf->stop1)!='')?$conf->stop1:'15:00');
	$start2 = strtotime((trim($conf->start2)!='')?$conf->start2:'19:00');
        $stop2 = strtotime((trim($conf->stop2)!='')?$conf->stop2:'22:00');
	$now = strtotime(date("H:i"));
	$ok_time = ($now>=$start1 && $now<=$stop1) || ($now>=$start2 && $now<=$stop2);
	if(isset($_REQUEST['kala_id']))
	{
		$out = array("factor_det"=>null,"error"=>"NotFound");
		if($ok_time)
		{
			$kala_id = (int)$_REQUEST['kala_id'];
			$tedad = (int)$_REQUEST['tedad'];
			$kala = new kala_class($kala_id);
			if(isset($kala->id) && $kala->mojoodi>=$tedad)
			{
				$ghimat = $tedad * $kala->ghimat;
				$det_id = factor_det_class::addFactorDet(-1*$user_id,$kala_id,$tedad,$ghimat);
				if($det_id>0)
				{
					$out['error'] = '';
					$out['factor_det'] = factor_class::loadDet($user_id);
				}
				else
					$out['error'] = 'DBError';
			}
			else if(isset($kala->id) && $kala->mojoodi<$tedad)
				$out['error'] = 'NoMojoodi';
		}
		else
			$out['error'] = 'NoTime';
		die(toJSON($out));
	}
	if(isset($_REQUEST['factor_det_id']))
	{
		
		$out = array("factor_det"=>null,"error"=>"NotFound");
		if($ok_time)
                {	
			$factor_det_id = (int)$_REQUEST['factor_det_id'];
			$my = new mysql_class;
			$out['query'] = "delete from `factor_det` where `id` = $factor_det_id";
			$ot = $my->ex_sqlx("delete from `factor_det` where `id` = $factor_det_id");
			$factor_det = factor_class::loadDet($user_id);
			$out['factor_det'] = $factor_det;
			if($ot == 'ok')
				$out['error'] = '';
		}
		else
			$out['error'] = 'NoTime';
		die(toJSON($out));
	}
	if(isset($_REQUEST['factor_done']))
        {
		if($ok_time)
                {
			$out = "0";
			$out = factor_class::finalFactor($user_id);
		}
		else
			$out = "-100";
                die("$out");
        }
	$user_obj = new user_class($user_id);
	$eshterak = $user_obj->fname.' '.$user_obj->lname.'['.$user_obj->eshterak.']';
	$user_user=$user_obj->user;
	$menu_arr = array();
	$m = new menu_class($user_id,$se);
	$menu_arr = $m->menu;
	$food_arr = kala_class::loadKalas();
	$factor_det = factor_class::loadDet($user_id);
	if(isset($_REQUEST['refreshDet']))
		die(toJSON($factor_det));
?>
<!doctype html>
<html lang="fa">
	<head>
		<meta charset="utf-8">
		<title><?php echo $conf->title;  ?></title>
		<link rel="stylesheet" href="../css/jquery-ui.css">
		<!-- <link rel="stylesheet" href="../css/style.css"> -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="../css/bootstrap.css" type="text/css" />
		<link rel="stylesheet" href="../css/bootstrap-responsive.min.css" type="text/css" />

		<link rel="stylesheet" href="../css/colorpicker.css">
		<link rel="stylesheet" href="../css/xgrid.css">
                <link rel="stylesheet" type="text/css" media="all" href="../js/cal/skins/aqua/theme.css" title="Aqua" />
		<script src="../js/jquery.min.js"></script>
		<script src="../js/jquery-ui.js"></script>
		<script src="../js/grid.js"></script>
		<script src="../js/date.js" ></script>
		<script src="../js/inc.js"></script>
		<script src="../js/colorpicker.js"></script>
		<script src="../js/md5.js"></script>
                <script type="text/javascript" src="../js/cal/jalali.js"></script>
                <script type="text/javascript" src="../js/cal/calendar.js"></script>
                <script type="text/javascript" src="../js/cal/calendar-setup.js"></script>
                <script type="text/javascript" src="../js/cal/lang/calendar-fa.js"></script>
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>
		<style>
			button
			{
				-webkit-border-radius: 4px;
				-moz-border-radius: 4px;
				border-radius: 4px;
				border:1px solid black;
				cursor : pointer;
			}
			button:hover
			{
				background-color:#fefefe;
			}
			.pointer
			{
				cursor:pointer;
			}
			.menu_item:hover,.menu_item_selected
			{
				background:yellow;
			}
		</style>
		<script>	
			var foods = <?php echo toJSON($food_arr); ?>;
			var menu_arr = <?php echo toJSON($menu_arr); ?>;
			var colCount = 5;
			var factor_det = <?php echo toJSON($factor_det); ?>;
			var factor_state = false;
			var ok_time = <?php echo $ok_time?'true':'false'; ?>;
			$(window).resize(function() {
				$("#factor_div").css("top","0px");
			});
			function sendFactor()
			{
				if(confirm('آیا فاکتور قطعی شود؟'))
				{
					$.get("index.php",{"factor_done":"factor_done"},function(result){
						var factor_id = parseInt(result,10);
						refreshSabad();
						if(factor_id>0 && !isNaN(factor_id))
							alert('شماره فاکتور شما '+factor_id+' می باشد');
						else
							alert('خطا در ثبت فاکتور');
					});
				}
			}
			function refreshSabad()
			{
				$("#factor_det").html("<img src='../status_fb.gif' />");
				$.getJSON("index.php",{"refreshDet":"refreshDet"},function(result){
					factor_det = result;
					drawFactor();		
				});
			}
			function animateAdd(id)
			{
				if($("#"+id).length>0)
				{
					var obj = $("#"+id);
					obj.animate({"left":"0px"}, "slow");
				}
			}
			function deleteKala(factor_det_id)
			{
				if(confirm('آیا از فاکتور حذف شود؟'))
					$.getJSON("index.php",{"factor_det_id":factor_det_id},function(result){
						console.log(result);
						if($.trim(result.error)=='')
						{
							factor_det = result.factor_det;
							drawFactor();
						}
						else
						{
							var err = $.trim(result.error);
							var tr_err = {
								'DBError':'خطا در سرور',
								'NotFound':'خطا در اطلاعات غذا'
							}
							alert(tr_err[err]);
						}
					});
			}
			function addKala(kala_id)
			{
				if(ok_time)
				{
					var tedad = parseInt($.trim(prompt('تعداد؟')),10);
					if(!isNaN(tedad) && tedad > 0)
					{
						$("#addPic_"+kala_id).prop('src','../img/status_fb.gif');
						$.getJSON("index.php",{"kala_id":kala_id,"tedad":tedad},function(result){
							$("#addPic_"+kala_id).prop('src','../img/plus.png');
							if($.trim(result.error)=='')
							{
								factor_det = result.factor_det;
								drawFactor();
								if(!factor_state)
								{
									toggleDet();
									setTimeout(function(){
										toggleDet();
									},2000);
								}
							}
							else
							{
								var err = $.trim(result.error);
								var tr_err = {
									'DBError':'خطا در سرور',
									'NoMojoodi':'عدم موجودی کافی',
									'NotFound':'خطا در اطلاعات غذا'
								}
								alert(tr_err[err]);
							}
						});
					}
				}
				else
					alert("در حال حاضر زمان سفارش نمی باشد");
			}
			function loadMenu(url,dobj)
			{
				if(url == 'admin_login.php')
				{
					if(confirm('آیا مایل به خروج هستید؟'))
						window.location="admin_login.php";
				}
				else if(url == 'index.php')
				{
					window.location="index.php";
				}
				else if(url == 'factor_det_all.php')
				{
					window.open("factor_det_all.php");
				}
				else
				{
					$(".khoon").remove();
					$(dobj).append("<img class='khoon' src='../img/status_fb.gif' />");
					$(".active").removeClass("active");
					$("#body").load(url, {limit: 25},function (responseText, textStatus, req) {
						$(dobj).addClass("active");
						$(".khoon").remove();
						if (textStatus == "error") {
							alert('خطا در دسترسی به سرور');
							return(false);
						}
					});
				}
			}
			function drawFactor()
			{
				var factor_div = $("#factor_det");
				var tedadKol = 0;
				var ghimatKol = 0;
				var out = '<table width="100%" cellspacing="0">';
				out += '<tr>';
				out += '<th colspan="4" style="background:#bbbbbb;">';
				out += 'پیش فاکتور خرید ';
				out += '</th>';
				out += '</tr>';
				out += '<tr>';
				out += '<th style="border:1px solid #000000;">ردیف</th>';
				out += '<th style="border:1px solid #000000;">شرح</th>';
				out += '<th style="border:1px solid #000000;">تعداد</th>';
				out += '<th style="border:1px solid #000000;">قیمت کل(ریال)</th>';
				out += '</tr>';
				for(i in factor_det)
				{
					out += '<tr>';
					out += '<td style="text-align:center;border:1px solid #000000;"><table width="100%"><tr><td align="left">'+String(parseInt(i,10)+1)+'</td><td align="right"><img width="20px" class="pointer" src="../img/delete.png" onclick="deleteKala('+factor_det[i].id+');" /></td></tr></table></td>';
					out += '<td style="border:1px solid #000000;">'+factor_det[i].kala.name+'</td>';
					out += '<td style="text-align:center;border:1px solid #000000;">'+String(factor_det[i].tedad)+'</td>';
					out += '<td style="text-align:center;border:1px solid #000000;">'+String(factor_det[i].ghimat)+'</td>';
					out += '</tr>';
					tedadKol += factor_det[i].tedad;
					ghimatKol += factor_det[i].ghimat;
				}
				out += '<tr style="background:#bbbbbb;">';
                                out += '<th colspan="2" style="border:1px solid #000000;">جمع : </th>';
                                out += '<th style="border:1px solid #000000;">'+tedadKol+'</th>';
                                out += '<th style="border:1px solid #000000;">'+ghimatKol+'</th>';
                                out += '</tr>';
				out += '<tr style="background:#bbbbbb;">';
				out += '<th colspan="4" style="border:1px solid #000000;"><button onclick="sendFactor();">ثبت نهایی</button></th>';
				out += '</tr>';
				out += '</table>';
				factor_div.html(out);
			}
			function drawMenu()
			{
				var menu_txt = '<ul class="nav nav-pills nav-tabs" >';
				for(i in menu_arr)
					menu_txt+='<li  class="'+((i>0)?'':'active')+'" onclick="loadMenu(\''+menu_arr[i].url+'\',this);"><a href="#" >'+menu_arr[i].name+'</a></li>';
				menu_txt+='</ul>';
				return(menu_txt);
			}
			function drawFoods(foods_tmp)
			{
				if(typeof foods_tmp == 'undefined' || foods_tmp == null)
					var foods_tmp = foods;
				var out = '';//'<table width="100%" style="text-align:center;">';
				out += '';//'<tr>';
				for(i in foods_tmp)
				{
					//if(i % colCount == 0 && i >0)
					//	out += '</tr><tr>';
					//out += '<td>';
					//out += '<table width="100%">';
					//out += '<tr>';
					//out += '<td colspan="2" style="text-align:center;">';
					out += '<div class="span2" style="height:310px;">';
					out += '<div class="thumbnail">';
					out += '<img style="cursor:pointer;" id="food_'+foods_tmp[i].id+'" src="'+foods_tmp[i].thumb+'" title="'+foods_tmp[i].name+'\nبرای مشاهده تصویر بزرگتر کلیک کنید" onclick="window.open(\''+foods_tmp[i].pic+'\');" width="180px" />';
					out += '</div>';
					//out += '</td>';
					//out += '</tr>';
					//out += '<tr>';
					//out += '<td title="برای اضافه کردن به سبد کلیک کنید" align="center" style="cursor:pointer;text-align:center;width:50%;" '+((foods_tmp[i].mojoodi>0)?'onclick="addKala('+foods_tmp[i].id+');"':'')+'>';
					out += '<div>';//+((foods_tmp[i].mojoodi<=0)?'class="disabled"':'')+' '+((foods_tmp[i].mojoodi>0)?'onclick="addKala('+foods_tmp[i].id+');"':'')+'>';
					var plusTmp = '<div class="alert alert-warning" >عدم موجودی</div>';
					if(foods_tmp[i].mojoodi>0)
						//plusTmp = '<img id="addPic_'+foods_tmp[i].id+'" src="../img/plus.png" width="10px" />';
						plusTmp = '<div class="btn btn-danger btn-block" onclick="addKala('+foods_tmp[i].id+');">سفارش</div>';
					//out += '</td>';
					//out += '<td title="برای اضافه کردن به سبد کلیک کنید" style="cursor:pointer;text-align:center;vertical-align:middle;" '+((foods_tmp[i].mojoodi>0)?'onclick="addKala('+foods_tmp[i].id+');"':'')+'>';
out += '<div '+((foods_tmp[i].mojoodi<=0)?'style="color:#bbbbbb;height:40px;"':'style="height:40px;"')+'>'+foods_tmp[i].name+'</div>'+'<div class="alert alert-success" >'+foods_tmp[i].ghimat+' ریال</div>';
					out += plusTmp;
					out += '</div>';
					out += '</div>';
					//out += '</td>';
					//out += '</tr>';
					//out += '</table>';
					//out += '</td>';
				}
				//out += '</tr></table>';
				$("#food_div").html(out);
			}
			function selectButton(obj)
			{
				if(obj)
				{
					var qobj = $(obj);
					$("button").css("border","#000000 solid 1px");
					qobj.css("border","dashed #666666 1px");
				}
			}
			function loadIndex()
			{
				window.location = 'index.php';
			}
			function loadCont(obj,addr)
			{
				//alert(addr);
				if(typeof obj == 'undefined')
					obj = document.getElementById('fastAccess');
				whereObj = {};
				$("#body").html("<img src='../img/status_fb.gif' >");
				$("#body").load(addr,function(){
                                        selectButton(obj);
                                });
			}
			function openDialog(addr,prop,fn)
			{
				$("#dialog").html("<img src='../img/status_fb.gif' alt='Loading . . .'/>");
				if($("#dialog").dialog)
				{
					if($("#dialog").dialog("isOpen"))
						$("#dialog").dialog("close");
					for(i in prop)
						$("#dialog").dialog("option",i,prop[i]);
					$("#dialog").dialog("open");
					$("#dialog").load(addr,function(){
						if(typeof fn == "function")
							fn();
					});
				}
			}
			function toggleDet()
			{
				if(factor_state)
				{
					$("#factor_kol").html('نمایش سبد خرید');
					$("#factor_det").slideUp();
				}
				else
				{
					$("#factor_kol").html('عدم نمایش سبد');
					$("#factor_det").slideDown();
				}
                                factor_state = !factor_state;
			}
			$(document).ready(function(){
				//loadCont(null,'fastAccess.php');
				drawFoods();
				$('#dialog').dialog({
						autoOpen : false,
						show: "slide",
						/*hide: "drop",*/
						modal: false,
						resizable: false,
						minWidth :200,
						minHeight : 200,
						position : 'center',
						closeOnEscape: true,
						beforeClose: function(event, ui) {
						return(true);
					}
				});
				$("#menu").html(drawMenu());
				drawFactor();
			});
		</script>
	</head>
	<body dir="rtl">
		<?php
			if($ok_time){
		?>
		<div id="factor_div" style="position:fixed;top:5px;left:5px;width:500px;">
			<div id="factor_det" style="display:none;border:green 1px solid;z-index:10;background:#ffffff;"></div>
			<div align="left">
				<div id="factor_kol" class="btn btn-primary" onclick="toggleDet();" style="cursor:pointer;">نمایش  سبد خرید </div>
			</div>
		</div>
		<?php
			}
		?>
		<div>
		        
			<div align="center" >
				<div class="row" >
					<div class="span9 text-right" style="padding-right:60px;padding-top:20px;" >
						<img src="../img/header.jpg">      
			       		</div>
					<div class="span3" style="padding-top:50px;" >
						<span class="alert alert-danger" >
						تلفن: 
							<?php echo $conf->tell; ?>
						</span>
					</div>
				</div>
				<div style="width:1104px">
					<div>
						<div id="menu" >
						</div>
						<div id="eshterak" class="alert alert-danger text-right" >
						کاربر 
							<?php echo $eshterak; ?>
						خوش آمدید
							<?php echo !$ok_time?' <span class="alert alert-warning">{در حال حاضر به علت خارج از زمان فعالیت اقاقیا بودن ، امکان سفارش ندارید ساعات فعالیت  '.$conf->start1.' - '.$conf->stop1.' و '.$conf->start2.' - '.$conf->stop2.'}</span>':''; ?>
						</div>
					</div>
					<div id="body" >
						<div id="food_div" ></div>
					</div>

				</div>
			</div>
		</div>
                <div id="dialog"></div>
	</body>
</html>

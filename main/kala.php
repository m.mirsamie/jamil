<?php
        include_once("../kernel.php");
        $SESSION = new session_class;
        register_shutdown_function('session_write_close');
        session_start();
	if(!isset($_SESSION[$conf->app.'_user_id']))
                die($conf->access_deny);
        $se = security_class::auth((int)$_SESSION[$conf->app.'_user_id']);
        if(!$se->can_view)
                die($conf->access_deny);
	$user_id = (int)$_SESSION[$conf->app.'_user_id'];
	$permission=array();
	$cl2=new mysql_class;
	//die("id is".$user_id);
	//$canWrite='';
	/*if($user_id==1)
		$canWrite='all';
	else
	{
		$cl2->ex_sql("select * from access_det where frase='doWrite'",$res2);
		if(count($res2)>0)
		{
		
			for($i=0;$i<count($res2);$i++)
			{
		
				$acc_id=$res2[$i]['acc_id'];
				$cl2->ex_sql("select  `page_name` from access where id='$acc_id' and group_id='$user_id'",$res3);
				if(count($res3)>0)
				{
					$p_name=$res3[0]['page_name'];
					$permission[]=$p_name;

				}

			}
		}
		if(in_array("kala.php",$permission))
			$canWrite='limited';
		
	}*/
	function loadGallery($id)
	{
		$id = (int)$id;
		$out = '----';
		$my = new mysql_class;
		$my->ex_sql("select `pic` from `kala` where `id` = '$id'",$q);
		if(isset($q[0]))
		{
			if ($q[0]["pic"]!='')
			{
				$pic = $q[0]["pic"];
				$out = "<img src=\"$pic\" class=\"imgGallTmb pointer\" onclick=\"startUpload('$id')\"/>";
				$out .= "<div id=\"fileupload_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
			else
			{
				$out = "<button  style=\"margin:10px;\" onclick=\"startUpload('$id');\">بروزرسانی تصویر</button>";
				$out .= "<div id=\"fileupload_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
		}		
		return($out);
	}
	function loadThumb($id)
	{
		$id = (int)$id;
		$out = '----';
		$my = new mysql_class;
		$my->ex_sql("select `thumb` as pic from `kala` where `id` = '$id'",$q);
		if(isset($q[0]))
		{
			if ($q[0]["pic"]!='')
			{
				$pic = $q[0]["pic"];
				$out = "<img src=\"$pic\" class=\"imgGallTmb pointer\" onclick=\"startUpload1('$id')\"/>";
				$out .= "<div id=\"fileupload1_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr1_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
			else
			{
				$out = "<button  style=\"margin:10px;\" onclick=\"startUpload1('$id');\">بروزرسانی تصویر</button>";
				$out .= "<div id=\"fileupload1_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr1_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
		}		
		return($out);
	}
	function tarikh($inp)
	{
		$out = ($inp=='0000-00-00 00:00:00' || $inp=='')? '':jdate("Y/m/d",strtotime($inp));
		return($out);
	}
	function utarikh($inp)
	{
		$out =hamed_pdateBack2($inp).' 23:59:59';
		return($out);
	}
	$columnList = '';
        $gname = 'grid1';
        $input =array($gname=>array('table'=>'kala','div'=>'main_div_kala'));
        $xgrid = new xgrid($input);
	$xgrid->column[$gname][0]['name']='';
	$xgrid->column[$gname][1]['name']='نام';
	$xgrid->column[$gname][1]['search'] = 'text';
	$xgrid->column[$gname][2]['name']='قیمت';
	$xgrid->column[$gname][3]['name']='';
	$xgrid->column[$gname][4]['name']='';
	$xgrid->column[$gname][5]['name']='موجودی';
	$xgrid->column[$gname][6]=$xgrid->column[$gname][0];
	$xgrid->column[$gname][6]['name']='تصویر کوچک';
	$xgrid->column[$gname][6]['cfunction']=array('loadThumb');
	$xgrid->column[$gname][6]['access']='a';

	$xgrid->column[$gname][7]=$xgrid->column[$gname][0];
        $xgrid->column[$gname][7]['name']='تصویر';
	$xgrid->column[$gname][7]['cfunction']=array('loadGallery');
	$xgrid->column[$gname][7]['access']='a';
	
	$xgrid->canAdd[$gname]= TRUE;
        $xgrid->canEdit[$gname]= TRUE;
        $xgrid->canDelete[$gname] = TRUE;
	
	$out =$xgrid->getOut($_REQUEST);
	if($xgrid->done)
        	die($out);	
?>
<script>
	var ggname = '<?php echo $gname; ?>';
	$(document).ready(function(){
                var args=<?php echo $xgrid->arg; ?>;
                intialGrid(args);
        });
	function loadPic(obj)
	{
		//alert(obj);
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load(obj,function(){
                        selectButton(obj);
                });
	}	
	function startUpload(id)
	{
		$("#ifr_"+id).prop("src","upload_pic.php?id="+id+"&");
		$("#fileupload_"+id).toggle();
	}
	function startUpload1(id)
	{
		$("#ifr1_"+id).prop("src","upload_pic.php?id="+id+"&field=thumb");
		$("#fileupload1_"+id).toggle();
	}
	function RPage()
	{
		$("#body").html("<img src='../img/status_fb.gif' >");
                $("#body").load('kala.php');
	}
</script>
<style>
	.imgGallTmb{width:50px;}
</style>
<div id="main_div_kala">
</div>
